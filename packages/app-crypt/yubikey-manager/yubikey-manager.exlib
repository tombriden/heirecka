# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=setuptools ]

SUMMARY="Python library and command line tool for configuring a YubiKey"

HOMEPAGE="https://developers.yubico.com/${PN}/"
# https://github.com/Yubico/yubikey-manager/issues/136
# ${PNV}-missing-test-files.tar created from test/files in the git repo
DOWNLOADS="
    ${HOMEPAGE}Releases/${PNV}.tar.gz
    https://dev.exherbo.org/distfiles/${PN}/${PNV}-missing-test-files.tar.xz
"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}Release_Notes.html"

LICENCES="BSD-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/click[python_abis:*(-)?]
        dev-python/cryptography[python_abis:*(-)?]
        dev-python/fido2[python_abis:*(-)?]
        dev-python/pyopenssl[python_abis:*(-)?]
        dev-python/pyscard[python_abis:*(-)?]
        dev-python/pyusb[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        python_abis:2.7? ( dev-python/enum34[python_abis:2.7] )
    run:
        sys-auth/yubikey-personalization
    test:
        python_abis:2.7? ( dev-python/mock[python_abis:2.7] )
"

BUGS_TO="heirecka@exherbo.org"

test_one_multibuild() {
    edo touch "${WORK}"/PYTHON_ABIS/$(python_get_abi)/${PNV}/test/__init__.py
    setup-py_test_one_multibuild
}

