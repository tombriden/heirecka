# Copyright 2013 Maxime Coste <frrrwww@gmail.com>
# Copyright 2015-2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever is_scm ; then
    SCM_REPOSITORY="https://git.tasktools.org/scm/tm/${PN}.git"
    require scm-git
else
    # Tests are not shipped with the tarball
    # (https://bug.tasktools.org/browse/TW-433), so we grab them from git:
    # git archive --format=tar v${PV} --prefix=task-${PV}/ test | xz -z > task-tests-${PV}.tar.xz
    DOWNLOADS="https://taskwarrior.org/download/${PNV}.tar.gz
               https://dev.exherbo.org/distfiles/${PN}/${PN}-tests-${PV}.tar.xz"
fi

require cmake [ api=2 ] bash-completion zsh-completion
require option-renames [ renames=[ 'gnutls sync' ] ]

export_exlib_phases src_compile src_install

SUMMARY="Taskwarrior is a command-line todo list manager."
HOMEPAGE="https://taskwarrior.org"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    sync [[ description = [ Share tasks between devices and sync them with a server ] ]]"

DEPENDENCIES="
    build:
        sync? ( virtual/pkg-config   [[ note = [ FindGnuTLS in cmake ] ]] )
    build+run:
        sys-apps/util-linux
        sync? ( dev-libs/gnutls )
    test:
        dev-lang/python:*[<3]
"

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=( SYNC )
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DTASK_DOCDIR=/usr/share/task
    -DTASK_MAN1DIR=/usr/share/man/man1
    -DTASK_MAN5DIR=/usr/share/man/man5
)

task_src_compile() {
    default

    expecting_tests && emake build_tests
}

task_src_install() {
    cmake_src_install

    # Don't install docs already installed by emagicdocs into a wrong dir
    edo rm "${IMAGE}"/usr/share/task/{AUTHORS,ChangeLog,COPYING,INSTALL,LICENSE,NEWS,README.md}

    dozshcompletion "${CMAKE_SOURCE}"/scripts/zsh/_task
    dobashcompletion "${CMAKE_SOURCE}"/scripts/bash/task.sh
}

